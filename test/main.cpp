#include <include/SysLog.h>
#include <gtest/gtest.h>

TEST(SysLog, LogEmpty) {
	std::string empty = "";

	ASSERT_FALSE(SysLog::Log(empty));
}

TEST(SysLog, LogNotEmpty) {
	std::string logStr = "This is a test";

	ASSERT_NO_THROW(SysLog::Log(logStr));
	ASSERT_NO_THROW(SysLog::Log(logStr, true, true));

	ASSERT_NO_THROW(SysLog::Log(SysLog::SeverityLevel::Alert, logStr));
}

int main() {
	testing::InitGoogleTest();
	return	RUN_ALL_TESTS();
}