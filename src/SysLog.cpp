#include "../include/SysLog.h"
#include <ctime>
#include <chrono>
#include <thread>
#include <sstream>
#include <fstream>
#include <mutex>

#ifdef __cpp_lib_filesystem
#include <filesystem>
namespace fs = std::filesystem;
#elif __cpp_lib_experimental_filesystem
#include <experimental/filesystem>
namespace fs = std::experimental::filesystem;
#else
#include <filesystem>
#pragma message "no filesystem support"
#endif

std::string LogFile = std::string(static_cast<char const *>(LOGFILE));
std::mutex fileMtx;

void SysLog::Init(){
#ifdef LOGTOFILE
#if defined(_cpp_lib_filesystem) || defined(__cpp_lib_experimental_filesystem)
	fs::path pathToLogFile = fs::path(LogFile);

	if (!fs::exists(pathToLogFile.parent_path()))
		fs::create_directories(pathToLogFile.parent_path());
#endif
#endif
}

bool SysLog::Log(const SeverityLevel severity, std::string const &msg, bool const reportThread, bool const newLine) {
	if (msg.empty())
		return false;

	std::string str = _getTimeStamp() + "  " +_getSeverityString(severity) + "  " + msg;

	return Log(str, reportThread, newLine);
}

bool SysLog::Log(std::string const &msg, bool const reportThread, bool const newLine) {

	std::lock_guard<std::mutex> guard(fileMtx);
	if (msg.empty())
		return false;

	std::string str = msg;
	if (reportThread)
		str = msg + " reported by thread " + _getThreadString();

	std::cout << str;
#ifdef LOGTOFILE
    _logToFile(str, newLine);
#endif
	if (newLine)
		LogEmptyLine();

	return true;
}

std::string SysLog::_getThreadString() {
	auto myid = std::this_thread::get_id();
	std::stringstream ss;
	ss << myid;

	return ss.str();
}

void SysLog::LogEmptyLine(size_t const nbLines) {
	for (size_t i = 0; i < nbLines; ++i)
		std::cout << NEWLINE;
}

std::string SysLog::_getTimeStamp() {
	std::time_t now = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());

	char buf[100] = { 0 };
	std::strftime(buf, sizeof(buf), "%Y-%m-%d %H:%M:%S", std::localtime(&now));
	return buf;
}

std::string SysLog::_getSeverityString(const SeverityLevel severity, bool const addPadding ) {
	std::string str;
	switch (severity)
	{
	case SeverityLevel::Emergency:
		str = "EMERGENCY";
		break;
	case SeverityLevel::Alert:
		str = "ALERT";
		break;
	case SeverityLevel::Error:
		str = "ERROR";
		break;
	case SeverityLevel::Warning:
		str ="WARNING";
		break;
	case SeverityLevel::Notice:
		str = "NOTICE";
		break;
	case SeverityLevel::Informational:
		str = "INFORMATIONAL";
		break;
	case SeverityLevel::Debug:
		str = "DEBUG";
		break;
	default:
		str = "UNKNOWN";
		break;
	}

	if (addPadding)
		_appendWhiteSpaces(str, MaxSeverityStringSize - str.size());

	return str;
}

void SysLog::_logToFile(const std::string &msg, bool const newLine) {
    std::ofstream outfile;

    outfile.open(LogFile, std::ios_base::app);
    outfile << msg;
    if (newLine)
        outfile << std::endl;
    outfile.close();
}

void SysLog::_appendWhiteSpaces(std::string &str, size_t const nb){
	for (size_t i = 0; i < nb;++i)
		str += " ";
}