#ifndef SYSLOG_H
#define SYSLOG_H

#include <iostream>
#include <string>

constexpr size_t MaxSeverityStringSize = 13;

#if defined(WIN32)|| defined(MINGW32) || defined(MINGW6)
#define NEWLINE "\r\n"
#else
#define NEWLINE "\n"
#endif

#define LOGTOFILE

#ifndef LOGFILE
#if defined(WIN32)|| defined(MINGW32) || defined(MINGW6)
#define LOGFILE "/var/log/syslogCI.log" 
#else
#define LOGFILE "C:\\syslogCI.log"
#endif // WIN32

#endif // LOGFILE


class SysLog{
public:

	enum class SeverityLevel : uint8_t {
		Emergency,
		Alert,
		Critical,
		Error,
		Warning,
		Notice,
		Informational,
		Debug,
		Roof = Debug
	};

	static void LogEmptyLine(size_t const nbLines = 1);
	static bool Log(SeverityLevel const severity, std::string const& msg, bool const reportThread = false, bool const newLine = true);
	static bool Log(std::string const & msg, bool const reportThread = false, bool const newLine = true);
    static void Init();

private:
    static void _logToFile(std::string const & msg, bool const newLine);
	static std::string _getTimeStamp();
	static std::string _getThreadString();
	static std::string _getSeverityString(const SeverityLevel severity, bool const addPadding = true);
	static void _appendWhiteSpaces(std::string &str, size_t const nb);

};
#endif // !SYSLOG_H
